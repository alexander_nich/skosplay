package fr.sparna.rdf.skos.toolkit.solr;

import org.apache.lucene.util.Version;

public interface LuceneVersion {

	public static Version VERSION = Version.LUCENE_43;
	
}
